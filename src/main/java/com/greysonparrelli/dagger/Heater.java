package com.greysonparrelli.dagger;

public interface Heater {
  void on();
  void off();
  boolean isHot();
}
