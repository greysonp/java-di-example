package com.greysonparrelli.dagger;

public interface Pump {
  void pump();
}
