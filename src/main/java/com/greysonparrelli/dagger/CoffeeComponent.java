package com.greysonparrelli.dagger;

import dagger.Component;
import javax.inject.Singleton;

@Component(modules = CoffeeModule.class)
@Singleton
public interface CoffeeComponent {
  CoffeeMaker maker();
}
