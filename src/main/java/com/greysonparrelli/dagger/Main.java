package com.greysonparrelli.dagger;


public class Main {

  public static void main(String[] args) {
    CoffeeComponent coffee = DaggerCoffeeComponent.builder().build();
    coffee.maker().brew();
  }
}
