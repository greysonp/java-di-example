package com.greysonparrelli.dagger;

import dagger.Provides;
import dagger.Module;
import javax.inject.Singleton;

@Module
public class CoffeeModule {

  @Provides
  @Singleton
  public Heater provideHeater() {
    return new ElectricHeater();
  }

  @Provides
  @Singleton
  public Pump providePump(Heater heater) {
    return new Thermosiphon(heater);
  }
}
