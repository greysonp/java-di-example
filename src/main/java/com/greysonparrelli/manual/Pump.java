package com.greysonparrelli.manual;

public interface Pump {
  void pump();
}
