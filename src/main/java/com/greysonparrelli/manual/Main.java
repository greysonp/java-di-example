package com.greysonparrelli.manual;

public class Main {

  public static void main(String[] args) {
    Heater heater = new ElectricHeater();
    Pump pump = new Thermosiphon(heater);
    CoffeeMaker coffeeMaker = new CoffeeMaker(heater, pump);
    coffeeMaker.brew();
  }
}
