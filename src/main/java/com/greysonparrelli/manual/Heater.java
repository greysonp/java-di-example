package com.greysonparrelli.manual;

public interface Heater {
  void on();
  void off();
  boolean isHot();
}
