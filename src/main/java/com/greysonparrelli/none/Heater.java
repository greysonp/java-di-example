package com.greysonparrelli.none;

public interface Heater {
  void on();
  void off();
  boolean isHot();
}
