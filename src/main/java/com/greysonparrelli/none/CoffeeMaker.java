package com.greysonparrelli.none;

class CoffeeMaker {
  private final Heater heater;
  private final Pump pump;

  CoffeeMaker() {
    this.heater = new ElectricHeater();
    this.pump = new Thermosiphon(this.heater);
  }

  public void brew() {
    heater.on();
    pump.pump();
    System.out.println(" [_]P coffee! [_]P ");
    heater.off();
  }
}
